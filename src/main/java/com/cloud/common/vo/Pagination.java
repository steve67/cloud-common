package com.cloud.common.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * Description : 通用分页返回对象  .
 *
 * @author : LuShunNeng
 * @date : Created in 2020/11/24 下午 7:49
 */
@Data
public class Pagination<T> implements Serializable {

    private static final long serialVersionUID = 7952047770347488974L;

    /**
     * 记录
     */
    private List<T> records;

    /**
     * 总条数
     */
    private long total;

    /**
     * 当前页大小
     */
    private long size;

    /**
     * 当前页
     */
    private long current;

    /**
     * 总页数
     */
    private long pages;

    /**
     * 是否有下一页
     */
    @Deprecated
    private Boolean isNext;

    /**
     * 是否有下一页
     */
    private Boolean hasNext;

    @Deprecated
    public Boolean getIsNext() {
        return current < pages;
    }

    public Boolean getHasNext() {
        return current < pages;
    }
}
