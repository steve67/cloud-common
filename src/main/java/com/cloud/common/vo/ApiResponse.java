package com.cloud.common.vo;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * create at  2018/07/19 13:49
 *
 * @author yanggang
 */
@Data
@ToString
public class ApiResponse<T> implements Serializable {

    private static final long serialVersionUID = 2177140286855555365L;


    private Integer errorCode = 0;


    private String errorStr;


    private T results;


    private String token;

    public ApiResponse() {
    }

    public ApiResponse(T results) {
        this.results = results;
    }
}
