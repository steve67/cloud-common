package com.cloud.common.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * Description : 通用分页请求对象  .
 *
 * @author : LuShunNeng
 * @date : Created in 2020/11/24 下午 7:49
 */
@Data
public class PaginationReq<T> implements Serializable {

    private static final long serialVersionUID = -1872960228398475561L;
    /**
     * 记录
     */
    private T pageSearchVo;

    /**
     * 查询页的索引 默认1
     */
    private long current = 1;

    /**
     * 查询页大小 默认10
     */
    private long size = 10;

}
