package com.cloud.common.model;

/**
 * create at  2018/08/27 13:36
 *
 * @author yanggang
 */
public interface IError {

    /**
     * 获取系统号
     *
     * @return 系统号
     */
    @Deprecated
    int getSystemNum();

    /**
     * 分层号
     * 0. 通用/基础异常
     * 1. 业务异常
     *
     * @return 获取
     */
    default int getLayerNum() {
        return 1;
    }

    /**
     * 具体错误码
     *
     * @return 字符串类型错误码
     */
    @Deprecated
    String getBaseCode();

    /**
     * 获取错误码
     *
     * @return 错误码
     */
    int getCode();

    /**
     * 获取错误描述
     *
     * @return 错误描述
     */
    String getDesc();

}
