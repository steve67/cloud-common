package com.cloud.common.model;

import com.cloud.common.util.PropertiesUtil;
import org.apache.commons.lang3.StringUtils;

/**
 * create at  2018/08/27 13:36
 *
 * @author yanggang
 */
public enum CommonError implements IError {
    /**
     * 通用异常类型
     */
    COMMON_BIZ_ERROR(101, "通用异常"),
    PARAM_ERROR(102, "参数错误"),
    TOKEN_ERROR(103, "鉴权失败异常"),

    AMQP_SEND_ERROR(901, "消息队列发送异常"),
    RATE_LIMIT(902, "请求过于频繁，请稍后再试"),
    DB_ERROR(903, "db异常"),
    CIRCUIT_BREAKER(904, "应用异常"),
    SYSTEM_ERROR(999, "系统异常"),;

    /**
     * 基本错误码
     */
    private int baseCode;

    /**
     * 错误描述
     */
    private String desc;

    CommonError(int baseCode, String desc) {
        this.baseCode = baseCode;
        this.desc = desc;
    }

    @Override
    public int getSystemNum() {
        return Integer.valueOf(PropertiesUtil.getProperty("com.cloud.system-num"));
    }

    @Override
    public int getLayerNum() {
        return 0;
    }

    @Override
    public String getBaseCode() {
        return StringUtils.leftPad(String.valueOf(this.baseCode), 3, "0");
    }

    @Override
    public int getCode() {
        return this.baseCode;
    }

    @Override
    public String getDesc() {
        return this.desc;
    }
}
