package com.cloud.common.model;

import java.io.Serializable;

/**
 * create at  2018/07/20 13:30
 *
 * @author yanggang
 */
@Deprecated
public class BizException extends RuntimeException implements Serializable {

    private static final long serialVersionUID = -7724389101946352429L;

    private IError iError;

    private Integer errorCode;

    public BizException() {
        super(CommonError.SYSTEM_ERROR.getDesc());
        this.iError = CommonError.SYSTEM_ERROR;
    }

    public BizException(IError iError) {
        super(iError.getDesc());
        this.iError = iError;
    }

    public BizException(IError iError, Throwable cause) {
        super(iError.getDesc(), cause != null && cause.getCause() != null ? cause.getCause() : cause);
        this.iError = iError;
    }

    public IError getError() {
        return iError;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }
}
