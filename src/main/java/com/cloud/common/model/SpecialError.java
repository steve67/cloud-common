package com.cloud.common.model;

/**
 * 特殊异常
 * 主要用于 客户端/前端 的部分特殊业务场景
 *
 * @author : frm
 * @date : Created in 2018/11/5 11:39 AM
 */
public enum SpecialError implements IError {
    /**
     * 特殊异常
     */
    USER_LOGIN_STATUS_ERROR(1401, "用户登录状态异常"),
    USER_LOGIN_STATUS_ERROR_NOTIFY(1402, "用户登录状态异常"),
    ERROR_USER_AUTH_EXPIRED(1403,"授权已过期，请重新授权"),
    DEVICE_ERROR(1500, "应用异常"),
    RATE_LIMIT(1600, "请求过于频繁，请稍后再试"),
    CIRCUIT_BREAKER(1601, "服务繁忙，请稍后再试"),

    ;



    /**
     * 基本错误码
     */
    private int baseCode;

    /**
     * 错误描述
     */
    private String desc;

    SpecialError() {
    }

    SpecialError(int baseCode, String desc) {
        this.baseCode = baseCode;
        this.desc = desc;
    }

    @Override
    public int getSystemNum() {
        return 0;
    }

    @Override
    public int getLayerNum() {
        return 0;
    }

    @Override
    public String getBaseCode() {
        return String.valueOf(this.baseCode);
    }

    @Override
    public int getCode() {
        return this.baseCode;
    }

    @Override
    public String getDesc() {
        return this.desc;
    }
}
