package com.cloud.common.model;

import java.io.Serializable;

/**
 * create at  2018/07/20 13:30
 *
 * @author yanggang
 */
@Deprecated
public class SystemException extends RuntimeException implements Serializable {

    private static final long serialVersionUID = -7724389101946352429L;

    private IError iError;

    private Integer errorCode;

    public SystemException() {
        super(CommonError.SYSTEM_ERROR.getDesc());
        this.iError = CommonError.SYSTEM_ERROR;
    }

    public SystemException(IError iError) {
        super(iError.getDesc());
        this.iError = iError;
    }

    public SystemException(IError iError, Integer errorCode, String message) {
        super(message);
        this.iError = iError;
        this.errorCode = errorCode;
    }

    public SystemException(IError iError, String message) {
        super(message);
        this.iError = iError;
    }

    public SystemException(IError iError, Throwable cause, String message) {
        super(message, cause != null && cause.getCause() != null ? cause.getCause() : cause);
        this.iError = iError;
    }

    public SystemException(IError iError, Throwable cause) {
        super(cause != null && cause.getCause() != null ? cause.getCause() : cause);
        this.iError = iError;
    }

    public IError getError() {
        return iError;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }
}
