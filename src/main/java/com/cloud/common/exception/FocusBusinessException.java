package com.cloud.common.exception;

import com.cloud.common.model.IError;
import com.cloud.common.model.SpecialError;

import java.io.Serializable;

/**
 * Huixian 业务异常
 *
 * @author : polegek
 * @date : Created in 2019-10-21 13:58
 */
public class FocusBusinessException extends BusinessException implements Serializable {


    private static final long serialVersionUID = -6053842488103687719L;

    /**
     * 默认错误为系统级错误
     */
    public FocusBusinessException() {
        super();
    }

    /**
     * 默认错误为系统级错误
     *
     * @param message   错误信息
     */
    public FocusBusinessException(String message) {
        super(message);
    }

    /**
     * 默认错误为系统级错误
     *
     * @param throwable 抛出内容
     */
    public FocusBusinessException(Throwable throwable) {
        super(throwable);
    }

    /**
     * 默认错误为系统级错误
     *
     * @param message   错误信息
     * @param throwable 抛出内容
     */
    public FocusBusinessException(String message, Throwable throwable) {
        super(message, throwable);
    }

    /**
     * 特殊错误
     * 针对特殊异常，直接返回错误码，不作任何处理
     *
     * @param specialError 特殊异常类型
     */
    public FocusBusinessException(SpecialError specialError) {
        super(specialError);
    }

    /**
     * 特殊错误
     * 针对特殊异常，直接返回错误码，不作任何处理
     *
     * @param specialError 特殊异常类型
     * @param message      错误信息
     */
    public FocusBusinessException(SpecialError specialError, String message) {
        super(specialError, message);
    }

    /**
     * 特殊错误
     * 针对特殊异常，直接返回错误码，不作任何处理
     *
     * @param specialError 特殊异常类型
     * @param throwable    抛出内容
     */
    public FocusBusinessException(SpecialError specialError, Throwable throwable) {
        super(specialError, throwable);
    }

    /**
     * 特殊错误
     * 针对特殊异常，直接返回错误码，不作任何处理
     *
     * @param specialError 特殊异常类型
     * @param message      错误信息
     * @param throwable    抛出内容
     */
    public FocusBusinessException(SpecialError specialError, String message, Throwable throwable) {
        super(specialError, message, throwable);
    }

    /**
     * 通用错误
     *
     * @param iError 通用错误类型
     */
    public FocusBusinessException(IError iError) {
        super(iError);
    }

    /**
     * 通用错误
     *
     * @param iError       通用错误类型
     * @param message      错误信息
     */
    public FocusBusinessException(IError iError, String message) {
        super(iError, message);
    }

    /**
     * 通用错误
     *
     * @param iError       通用错误类型
     * @param throwable    抛出内容
     */
    public FocusBusinessException(IError iError, Throwable throwable) {
        super(iError, throwable);
    }

    /**
     * 通用错误
     *
     * @param iError       通用错误类型
     * @param message      错误信息
     * @param throwable    抛出内容
     */
    public FocusBusinessException(IError iError, String message, Throwable throwable) {
        super(iError, message, throwable);
    }
}
