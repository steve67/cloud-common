package com.cloud.common.exception;

import java.io.Serializable;

/**
 * Huixian 业务异常
 *
 * @author : polegek
 * @date : Created in 2019-10-21 13:58
 */
public class ApplicationSystemException extends RuntimeException implements Serializable {


    private static final long serialVersionUID = 3141189612882179936L;

    /**
     * 默认错误为系统级错误
     */
    public ApplicationSystemException() {
        super();
    }

    /**
     * 默认错误为系统级错误
     *
     * @param message   错误信息
     */
    public ApplicationSystemException(String message) {
        super(message);
    }

    /**
     * 默认错误为系统级错误
     *
     * @param throwable 抛出内容
     */
    public ApplicationSystemException(Throwable throwable) {
        super(throwable);
    }

    /**
     * 默认错误为系统级错误
     *
     * @param message   错误信息
     * @param throwable 抛出内容
     */
    public ApplicationSystemException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
