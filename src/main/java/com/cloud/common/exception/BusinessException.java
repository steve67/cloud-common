package com.cloud.common.exception;


import com.cloud.common.model.CommonError;
import com.cloud.common.model.IError;
import com.cloud.common.model.SpecialError;

import java.io.Serializable;

/**
 * Huixian 异常公共类
 *
 * @author : polegek
 * @date : Created in 2019-10-21 13:58
 */
public class BusinessException extends RuntimeException implements Serializable {

    private static final long serialVersionUID = -871018851896931L;

    /**
     * 项目异常码
     */
    private int baseCode = 0;

    /**
     * 基础错误信息
     */
    private String baseMsg = "";

    /**
     * 分层错误码
     */
    private int layer = 0;

    /**
     * 响应错误码
     */
    private int errorCode = 0;

    /**
     * 默认错误为系统级错误
     */
    public BusinessException() {
        this(CommonError.SYSTEM_ERROR.getDesc());
    }

    /**
     * 默认错误为系统级错误
     *
     * @param message   错误信息
     */
    public BusinessException(String message) {
        this(message, null);
    }

    /**
     * 默认错误为系统级错误
     *
     * @param throwable 抛出内容
     */
    public BusinessException(Throwable throwable) {
        this(CommonError.SYSTEM_ERROR.getDesc(), throwable);
    }

    /**
     * 默认错误为系统级错误
     *
     * @param message   错误信息
     * @param throwable 抛出内容
     */
    public BusinessException(String message, Throwable throwable) {
        super(message, throwable);
        setBaseCode(CommonError.SYSTEM_ERROR.getCode());
        setBaseMsg(CommonError.SYSTEM_ERROR.getDesc());
        setLayer(CommonError.SYSTEM_ERROR.getLayerNum());
    }

    /**
     * 特殊错误
     * 针对特殊异常，直接返回错误码，不作任何处理
     *
     * @param specialError 特殊异常类型
     */
    public BusinessException(SpecialError specialError) {
        this(specialError, specialError.getDesc());
    }

    /**
     * 特殊错误
     * 针对特殊异常，直接返回错误码，不作任何处理
     *
     * @param specialError 特殊异常类型
     * @param message      错误信息
     */
    public BusinessException(SpecialError specialError, String message) {
        this(specialError, message, null);
    }

    /**
     * 特殊错误
     * 针对特殊异常，直接返回错误码，不作任何处理
     *
     * @param specialError 特殊异常类型
     * @param throwable    抛出内容
     */
    public BusinessException(SpecialError specialError, Throwable throwable) {
        this(specialError, specialError.getDesc(), throwable);
    }

    /**
     * 特殊错误
     * 针对特殊异常，直接返回错误码，不作任何处理
     *
     * @param specialError 特殊异常类型
     * @param message      错误信息
     * @param throwable    抛出内容
     */
    public BusinessException(SpecialError specialError, String message, Throwable throwable) {
        super(message, throwable);
        setBaseCode(specialError.getCode());
        setLayer(specialError.getLayerNum());
        setBaseMsg(specialError.getDesc());
        setErrorCode(specialError.getCode());
    }

    /**
     * 通用错误
     *
     * @param iError 通用错误类型
     */
    public BusinessException(IError iError) {
        this(iError, iError.getDesc());
    }

    /**
     * 通用错误
     *
     * @param iError       通用错误类型
     * @param message      错误信息
     */
    public BusinessException(IError iError, String message) {
        this(iError, message, null);
    }

    /**
     * 通用错误
     *
     * @param iError       通用错误类型
     * @param throwable    抛出内容
     */
    public BusinessException(IError iError, Throwable throwable) {
        this(iError, iError.getDesc(), throwable);
    }

    /**
     * 通用错误
     *
     * @param iError       通用错误类型
     * @param message      错误信息
     * @param throwable    抛出内容
     */
    public BusinessException(IError iError, String message, Throwable throwable) {
        super(message, throwable);
        setBaseCode(iError.getCode());
        setBaseMsg(iError.getDesc());
        setLayer(iError.getLayerNum());
    }

    public int getBaseCode() {
        return baseCode;
    }

    public void setBaseCode(int baseCode) {
        this.baseCode = baseCode;
    }

    public String getBaseMsg() {
        return baseMsg;
    }

    public void setBaseMsg(String baseMsg) {
        this.baseMsg = baseMsg;
    }

    public int getLayer() {
        return layer;
    }

    public void setLayer(int layer) {
        this.layer = layer;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }
}
