/**
 * HuiXian Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package com.cloud.common.util;
/**
 * 环境工具类
 *
 * @author LiuYi
 * @version $Id: EnvironmentUtil.java, v 0.1 2016年5月25日 下午8:37:03 LiuYi Exp $
 */
public class EnvironmentUtil {

    /**
     * 获取环境标识
     *
     * @return
     */
    public static String getEnvFlag() {
        return PropertiesUtil.getEnvironment().getActiveProfiles()[0];
    }

    /**
     * 是否线上产品环境
     *
     * @return
     */
    public static boolean isPrd() {
        return "prd".equalsIgnoreCase(getEnvFlag());
    }

    /**
     * 是否预发布环境
     *
     * @return
     */
    public static boolean isPre() {
        return "pre".equalsIgnoreCase(getEnvFlag());
    }

    /**
     * 是否测试环境
     *
     * @return
     */
    public static boolean isTest() {
        return "test".equalsIgnoreCase(getEnvFlag());
    }

    /**
     * 是否开发环境
     *
     * @return
     */
    public static boolean isDev() {
        return "dev".equalsIgnoreCase(getEnvFlag());
    }

    /**
     * 是否本地环境
     *
     * @return
     */
    public static boolean isLoc() {
        return "loc".equalsIgnoreCase(getEnvFlag());
    }

    /**
     * 是否沙箱环境
     *
     * @return
     */
    public static boolean isSandbox() {
        return "sandbox".equalsIgnoreCase(getEnvFlag());
    }

    /**
     * 非正式和预发布
     *
     * @return
     */
    public static boolean isNotPrdPre() { return !(isPrd() || isPre()); }
    
    /**
     * 本地和开发
     *
     * @return
     */
    public static boolean isLocOrDev() { return (isLoc() || isDev()); }
}
