/*
 * Copyright (c) 2018. Polegek
 */

package com.cloud.common.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Json Web Token 工具
 *
 * @author : polegek
 * @date : Created in 2018/11/16 0:40
 */
public class JwtUtil {

    /**
     * 生成 JWT
     *
     * @param claims Payload中的参数
     * @param privateKey 私钥
     *
     * @return .
     *
     * @throws IOException .
     * @throws InvalidKeySpecException .
     * @throws NoSuchAlgorithmException .
     * @throws JWTCreationException .
     */
    public static String genToken(Map<String, String> claims, String privateKey)
            throws IOException, InvalidKeySpecException, NoSuchAlgorithmException, JWTCreationException {
        Algorithm algorithm = null;

        algorithm = Algorithm.RSA256(null, (RSAPrivateKey) AlgorithmUtil.Rsa.getPrivateKey(privateKey));

        //开始构建JWT，设置创建时间
        JWTCreator.Builder builder = JWT.create().withIssuedAt(new Date());

        //然后把传入的claims放进builder里面，这里使用了java8的方法引用
        claims.forEach(builder::withClaim);

        //签名之后返回
        return builder.sign(algorithm);
    }

    /**
     * verify 签名验证
     *
     * @param token JWT
     *
     * @return .
     * @throws IOException .
     * @throws JWTVerificationException .
     * @throws InvalidKeySpecException .
     * @throws NoSuchAlgorithmException .
     */
    public static Map<String, String> verifyToken(String token,String publicKey) throws IOException, JWTVerificationException, InvalidKeySpecException, NoSuchAlgorithmException {
        Algorithm algorithm ;

        //根据公钥生成算法对象
        algorithm = Algorithm.RSA256((RSAPublicKey) AlgorithmUtil.Rsa.getPublicKey(publicKey),null);

        //根据算法生成验证对象
        JWTVerifier verifier = JWT.require(algorithm).build();
        DecodedJWT jwt = null;

        //验证
        jwt =  verifier.verify(token);
        Map<String, Claim> map = jwt.getClaims();
        Map<String, String> resultMap = new HashMap<>();
        map.forEach((k,v) -> resultMap.put(k, v.asString()));

        //iat添加到payload
        if(jwt.getIssuedAt()!=null){
            resultMap.put("iat", String.valueOf(jwt.getIssuedAt().getTime() / 1000));
        }

        return resultMap;
    }
}
