package com.cloud.common.util;

import java.util.EnumMap;
import java.util.function.Function;

/**
 * @author kkyv
 * @Date 2020/3/18 1:09 PM
 * @Created by kkyv
 */
public class EnumUtil {

    /**
     * 枚举转换工具类
     *
     * scene: 在页面上 要显示
     *      状态列表: PROCESS->处理中，SUCCESS->处理成功
     *
     * eg : EnumMap<Status, String> statusMap = EnumUtil.enumStringMap(Status.class, Status::getDesc);
     *
     * @param keyType 需要被转换的枚举类
     * @param consumer 需要被转换的枚举值
     * @param <K> 枚举的类型
     * @return 被转换的结果map
     */
    public static <K extends Enum<K>> EnumMap<K, String> enumStringMap(Class<K> keyType, Function<K, String> consumer) {

        EnumMap<K, String> enumMap = new EnumMap<>(keyType);

        K[] enumConstants = keyType.getEnumConstants();
        for (K enumConstant : enumConstants) {
            enumMap.put(enumConstant, consumer.apply(enumConstant));
        }

        return enumMap;
    }

    /**
     * 枚举转换工具类
     *
     * @param keyType 需要被转换的枚举类
     * @param consumer 需要被转换的枚举值
     * @param <K> 枚举的类型
     * @return 被转换的结果map
     */
    public static <K extends Enum<K>> EnumMap<K, Object> enumObjMap(Class<K> keyType, Function<K, Object> consumer) {


        EnumMap<K, Object> enumMap = new EnumMap<>(keyType);

        K[] enumConstants = keyType.getEnumConstants();
        for (K enumConstant : enumConstants) {
            enumMap.put(enumConstant, consumer.apply(enumConstant));
        }

        return enumMap;
    }
}
