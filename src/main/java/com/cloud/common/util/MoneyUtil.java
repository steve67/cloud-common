/**
 * HuiXian Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package com.cloud.common.util;

import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 金钱工具类
 *
 * @author LiuYi
 * @version $Id: PriceUtil.java, v 0.1 2016年3月22日 上午11:21:13 LiuYi Exp $
 */
public class MoneyUtil {

    /**
     * 将元为单位的转换为分
     * 替换小数点
     *
     * @param amount
     * @return
     */
    public static Integer convertYuanToFen(String amount) {
        Integer amLong = 0;
        if (StringUtils.isEmpty(amount)) {
            amount = "";
        }
        int index = amount.indexOf(".");
        int length = amount.length();

        if (index == -1) {
            amLong = Integer.valueOf(amount + "00");
        } else if (length - index >= 3) {
            amLong = Integer.valueOf((amount.substring(0, index + 3)).replace(".", ""));
        } else if (length - index == 2) {
            amLong = Integer.valueOf((amount.substring(0, index + 2)).replace(".", "") + 0);
        } else {
            amLong = Integer.valueOf((amount.substring(0, index + 1)).replace(".", "") + "00");
        }
        return amLong;
    }

    /**
     * 分转换为元.  用来显示的逗号隔开的,包含小数的
     *
     * @param fen 分
     * @return 元
     */
    public static String convertFenToYuanDecimal(final String fen) {
        String yuan = "0.00";
        StringBuffer result = new StringBuffer();
        String transFen = fen;
        boolean isMinus = fen.startsWith("-");
        if (isMinus) {
            transFen = fen.substring(1, fen.length());
        }
        final int MULTIPLIER = 100;
        Pattern pattern = Pattern.compile("^[1-9][0-9]*{1}");
        Matcher matcher = pattern.matcher(transFen);
        if (matcher.matches()) {
            yuan = new BigDecimal(transFen).divide(new BigDecimal(MULTIPLIER)).setScale(2).toString();
            String intString = yuan.substring(0, yuan.length() - 3);
            for (int i = 1; i <= intString.length(); i++) {
                if ((i - 1) % 3 == 0 && i != 1) {
                    result.append(",");
                }
                result.append(
                        intString.substring(intString.length() - i, intString.length() - i + 1));
            }
            result.reverse().append(".").append(yuan.substring(yuan.length() - 2));
            yuan = result.toString();
            if (isMinus) {
                yuan = "-" + yuan;
            }
        } else {
            yuan = "0.00";
        }
        return yuan;
    }

    public static String convertFenToYuanDecimal(final Integer fen) {
        return convertFenToYuanDecimal(fen.toString());
    }

    /**
     * 分转换为元.  用来显示的逗号隔开的,不包含小数的
     *
     * @param fen 分
     * @return 元
     */
    public static String convertFenToYuanNoDecimal(final String fen) {
        //元不要带小数点
        return StringUtils.replace(convertFenToYuanDecimal(fen), ".00", "");
    }

    /**
     * 分转换为元.  没有逗号隔开，用于计算
     *
     * @param fen 分
     * @return 元
     */
    public static String convertToYuan(final String fen) {
        String yuan = "0.00";
        StringBuffer result = new StringBuffer();
        String transFen = fen;
        boolean isMinus = fen.startsWith("-");
        if (isMinus) {
            transFen = fen.substring(1, fen.length());
        }
        final int MULTIPLIER = 100;
        Pattern pattern = Pattern.compile("^[1-9][0-9]*{1}");
        Matcher matcher = pattern.matcher(transFen);
        if (matcher.matches()) {
            yuan = new BigDecimal(transFen).divide(new BigDecimal(MULTIPLIER)).setScale(2)
                    .toString();
            //            result.reverse().append(".").append(yuan.substring(yuan.length() - 2));
            //            yuan = result.toString();
            if (isMinus) {
                yuan = "-" + yuan;
            }
        } else {
            yuan = "0";
        }
        //元后为全0的去掉
        yuan = StringUtils.replace(yuan, ".00", "");
        return yuan;
    }

    /**
     * 分向上取整,返回值没有角和分的值
     * 例如：210 返回 300
     *
     * @param amount
     * @return
     */
    public static Integer fenCeil(String amount) {
        String yuan = BigDecimal.valueOf(Integer.valueOf(amount)).divide(new BigDecimal(100))
                .toString();
        Double yuanDouble = Double.valueOf(yuan);
        yuanDouble = NumberUtil.ceil(yuanDouble);
        return convertYuanToFen(String.valueOf(yuanDouble));
    }

    /**
     * 字符型的金额转换成BigDecimal类型的金额类型.
     *
     * @param amount
     * @return
     */
    public static BigDecimal toBigDecimalByString(String amount) {
        BigDecimal bd = new BigDecimal(amount);
        bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
        return bd;
    }
}
