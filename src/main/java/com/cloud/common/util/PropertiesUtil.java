package com.cloud.common.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * create at  2018/08/13 14:48
 *
 * @author yanggang
 */
@Component
public class PropertiesUtil implements ApplicationContextAware {

    /**
     * Spring应用上下文环境
     */
    private static ApplicationContext applicationContext;

    private static Environment environment;

    public static String getProperty(String key) {
        return environment.getProperty(key);
    }

    public static ApplicationContext getApplicationContext() {
        return PropertiesUtil.applicationContext;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        PropertiesUtil.applicationContext = applicationContext;
        PropertiesUtil.environment = applicationContext.getEnvironment();
    }


    public static Environment getEnvironment() {
        return PropertiesUtil.environment;
    }
}
