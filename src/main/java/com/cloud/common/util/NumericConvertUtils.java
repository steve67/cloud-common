package com.cloud.common.util;


/**
 * Description : 数据进制转换util  .
 *
 * @author : DuanBingfang
 * @date : Created in 2019/7/12 14:26
 */
public class NumericConvertUtils {

    /**
     * 在进制表示中的字符集合，0-Z分别用于表示最大为62进制的符号表示
     */
    private static final char[] digits = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a',
            'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
            's',
            't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
            'K',
            'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};

    /**
     * 无 0、I、L、O
     */
    private static final char[] digits32 = {'1', '2', '3', '4', '5', '6', '7', '8', '9',
            'A',
            'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U',
            'V',
            'W', 'X', 'Y', 'Z'};

    /**
     * 无 0、l、I、O
     */
    private static final char[] digits58 = {'1', '2', '3', '4', '5', '6', '7', '8', '9', 'a',
            'b',
            'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
            'u',
            'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M',
            'N',
            'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};

    /**
     * 将十进制数据转换为指定的进制
     * @param number  十进制数据
     * @param seed    转换进制
     * @return  .
     */
    public static String toOtherNumberSystem(long number, int seed) {
        if (number < 0) {
            number = ((long) 2 * 0x7fffffff) + number + 2;
        }
        char[] buf = new char[32];
        int charPos = 32;
        while ((number / seed) > 0) {
            buf[--charPos] = digits[(int) (number % seed)];
            number /= seed;
        }
        buf[--charPos] = digits[(int) (number % seed)];
        return new String(buf, charPos, (32 - charPos));
    }

    /**
     * 将64进制转换为指定进制
     * @param number    字符串数据
     * @param seed      转换进制
     * @return .
     */
    public static long toDecimalNumber(String number, int seed) {
        char[] charBuf = number.toCharArray();
        if (seed == 10) {
            return Long.parseLong(number);
        }

        long result = 0, base = 1;

        for (int i = charBuf.length - 1; i >= 0; i--) {
            int index = 0;
            for (int j = 0, length = digits.length; j < length; j++) {
                // 找到对应字符的下标，对应的下标才是具体的数值
                if (digits[j] == charBuf[i]) {
                    index = j;
                }
            }
            result += index * base;
            base *= seed;
        }
        return result;
    }

    /**
     * 将十进制数据转换为32进制数据  无 0、I、L、O
     * @param number    十进制数据
     * @return .
     */
    public static String toOtherNumberSystem32(long number) {
        int seed = 32;
        if (number < 0) {
            number = ((long) 2 * 0x7fffffff) + number + 2;
        }
        char[] buf = new char[32];
        int charPos = 32;
        while ((number / seed) > 0) {
            buf[--charPos] = digits32[(int) (number % seed)];
            number /= seed;
        }
        buf[--charPos] = digits32[(int) (number % seed)];
        return new String(buf, charPos, (32 - charPos));
    }

    /**
     *  将32进制数据转换为十进制的数据
     * @param number    32进制数据
     * @return .
     */
    public static long toDecimalNumber32(String number) {
        int seed = 32;
        char[] charBuf = number.toCharArray();
        long result = 0, base = 1;

        for (int i = charBuf.length - 1; i >= 0; i--) {
            int index = 0;
            for (int j = 0, length = digits32.length; j < length; j++) {
                // 找到对应字符的下标，对应的下标才是具体的数值
                if (digits32[j] == charBuf[i]) {
                    index = j;
                }
            }
            result += index * base;
            base *= seed;
        }
        return result;
    }

    /**
     * 将十进制转换为58进制
     * @param number    十进制数据
     * @return .
     */
    public static String toOtherNumberSystem58(long number) {
        int seed = 58;
        if (number < 0) {
            number = ((long) 2 * 0x7fffffff) + number + 2;
        }
        char[] buf = new char[32];
        int charPos = 32;
        while ((number / seed) > 0) {
            buf[--charPos] = digits58[(int) (number % seed)];
            number /= seed;
        }
        buf[--charPos] = digits58[(int) (number % seed)];
        return new String(buf, charPos, (32 - charPos));
    }

    /**
     * 将58进制转换为十进制
     * @param number    58进制数据
     * @return .
     */
    public static long toDecimalNumber58(String number) {
        int seed = 58;
        char[] charBuf = number.toCharArray();
        long result = 0, base = 1;

        for (int i = charBuf.length - 1; i >= 0; i--) {
            int index = 0;
            for (int j = 0, length = digits58.length; j < length; j++) {
                // 找到对应字符的下标，对应的下标才是具体的数值
                if (digits58[j] == charBuf[i]) {
                    index = j;
                }
            }
            result += index * base;
            base *= seed;
        }
        return result;
    }

}
