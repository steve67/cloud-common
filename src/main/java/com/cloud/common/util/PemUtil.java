/*
 * Copyright (c) 2018. Polegek
 */

package com.cloud.common.util;

import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemReader;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.EncodedKeySpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

/**
 * Pem文件 生成 Rsa公私钥
 *
 * @author : polegek
 * @date : Created in 2018/11/16 0:46
 */
@Deprecated
public class PemUtil {

    public static final String KEY_ALGORITHM = "RSA";
    private static PrivateKey PRIVATE_KEY = null;
    private static PublicKey PUBLIC_KEY = null;

    /**
     * 读取file
     *
     * @param pemFile .
     *
     * @return .
     *
     * @throws IOException .
     */
    private static byte[] parsePEMFile(File pemFile) throws IOException {
        if (!pemFile.isFile() || !pemFile.exists()) {

        }

        byte[] content = null;
        try (PemReader reader = new PemReader(new FileReader(pemFile))) {
            PemObject pemObject = reader.readPemObject();
            content = pemObject.getContent();
        }

        return content;
    }

    /**
     * 根据文件二进制获取 公钥对象
     *
     * @param keyBytes  二进制公钥
     * @param algorithm 算法
     *
     * @return .
     *
     * @throws NoSuchAlgorithmException .
     * @throws InvalidKeySpecException .
     */
    private static PublicKey getPublicKeyByBytes(byte[] keyBytes, String algorithm) throws NoSuchAlgorithmException, InvalidKeySpecException {
        PublicKey publicKey = null;
        KeyFactory kf = KeyFactory.getInstance(algorithm);
        EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
        publicKey = kf.generatePublic(keySpec);

        return publicKey;
    }

    /**
     * 根据文件二进制获取 私钥对象
     *
     * @param keyBytes  二进制公钥
     * @param algorithm 算法
     *
     * @return .
     *
     * @throws NoSuchAlgorithmException .
     * @throws InvalidKeySpecException .
     */
    private static PrivateKey getPrivateKeyByBytes(byte[] keyBytes, String algorithm) throws NoSuchAlgorithmException, InvalidKeySpecException {
        PrivateKey privateKey = null;
        KeyFactory kf = KeyFactory.getInstance(algorithm);
        EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(keyBytes);
        privateKey = kf.generatePrivate(keySpec);

        return privateKey;
    }

    /**
     * 根据字符串 获取公钥
     *
     * @return .
     *
     * @throws IOException .
     * @throws InvalidKeySpecException .
     * @throws NoSuchAlgorithmException .
     */
    public static PublicKey getPublicKey(String publicKeyFile) throws IOException, InvalidKeySpecException, NoSuchAlgorithmException {
        if (PUBLIC_KEY == null){
            byte[] bytes = PemUtil.parsePEMFile(new File(publicKeyFile) );
            PUBLIC_KEY = PemUtil.getPublicKeyByBytes(bytes, KEY_ALGORITHM);
        }
        return PUBLIC_KEY;
    }

    /**
     * 根据字符串 获取私钥
     *
     * @return .
     *
     * @throws IOException .
     * @throws InvalidKeySpecException .
     * @throws NoSuchAlgorithmException .
     */
    public static PrivateKey getPrivateKey(String privateKeyFile) throws IOException, InvalidKeySpecException, NoSuchAlgorithmException {
        if (PRIVATE_KEY == null){
            byte[] bytes = PemUtil.parsePEMFile(new File(privateKeyFile) );
            PRIVATE_KEY = PemUtil.getPrivateKeyByBytes(bytes, KEY_ALGORITHM);
        }
        return PRIVATE_KEY;
    }
}
