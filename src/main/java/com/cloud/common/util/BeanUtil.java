package com.cloud.common.util;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SimplePropertyPreFilter;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * CreateBy KKYV 2018/9/19
 */
public class BeanUtil {

    /**
     * 使用springUtils拷贝属性
     * 不支持字段和枚举类转换
     *
     * @param source 源对象
     * @param target 目标对象的class
     * @return 目标对象实例
     */
    public static <T> T copyPropertiesBySpring(Object source, Class<T> target) {
        if (source == null) {
            return null;
        }
        try {
            T t = target.newInstance();
            BeanUtils.copyProperties(source, t);
            return t;
        } catch (InstantiationException | IllegalAccessException e) {
            // 一般不会出现异常
        }
        return null;
    }

    /**
     * 使用springUtils拷贝属性
     * 不支持字段和枚举类转换
     *
     * @param source 源对象列表
     * @param target 目标对象的class
     * @return 目标对象实例列表
     */
    public static <T> List<T> copyPropertiesBySpring(List<?> source, Class<T> target) {
        if (source == null) {
            return null;
        }
        return source.stream().map(item -> copyPropertiesBySpring(item, target))
                .collect(Collectors.toList());
    }

    /**
     * 使用springUtils拷贝属性
     * 不支持字段和枚举类转换
     *
     * @param source 源对象
     * @param target 目标对象的class
     * @return 目标对象实例
     */
    public static <T> T copyPropertiesBySpring(Object source, Class<T> target,
            String[] ignoreProperties) {
        if (source == null) {
            return null;
        }
        try {
            T t = target.newInstance();
            BeanUtils.copyProperties(source, t, ignoreProperties);
            return t;
        } catch (InstantiationException | IllegalAccessException e) {
            // 一般不会出现异常
        }
        return null;
    }

    /**
     * 使用springUtils拷贝属性
     * 不支持字段和枚举类转换
     *
     * @param source 源对象列表
     * @param target 目标对象的class
     * @return 目标对象实例列表
     */
    public static <T> List<T> copyPropertiesBySpring(List<?> source, Class<T> target,
            String[] ignoreProperties) {
        if (source == null) {
            return null;
        }
        return source.stream().map(item -> copyPropertiesBySpring(item, target, ignoreProperties))
                .collect(Collectors.toList());
    }

    /**
     * 使用fastJson，转换为json字符串再拷贝属性
     * 支持字段和枚举类转换
     *
     * @param source 源对象
     * @param target 目标对象的class
     * @return 目标对象实例
     */
    public static <T> T copyPropertiesByFastJson(Object source, Class<T> target) {
        if (source == null) {
            return null;
        }
        String sourceStr = JSONObject.toJSONString(source);
        return JSONObject.parseObject(sourceStr, target);
    }


    /**
     * 使用fastJson，转换为json字符串再拷贝属性
     * 支持字段和枚举类转换
     *
     * @param source 源对象
     * @param target 目标对象的class
     * @param ignoreProperties 忽略的对象
     * @return 目标对象实例
     */
    @Deprecated
    public static <T> T copyPropertiesByFastJson(Object source, Class<T> target,String[] ignoreProperties) {
        if (source == null){
            return null;
        }
        List<String> ignorePropertiesList = new ArrayList<>();
        if (ignoreProperties != null){
            for (String str:ignoreProperties) {
                ignorePropertiesList.add(str);
            }

        }
        return copyPropertiesByFastJson(source,target,ignorePropertiesList);
    }

    /**
     * 使用fastJson，转换为json字符串再拷贝属性
     * 支持字段和枚举类转换
     *
     * @param source 源对象
     * @param target 目标对象的class
     * @param ignoreProperties 忽略的对象
     * @return 目标对象实例
     */
    public static <T> T copyPropertiesByFastJson(Object source, Class<T> target,List<String> ignoreProperties) {
        if (source == null){
            return null;
        }
        if (ignoreProperties.size() == 0){
            return copyPropertiesByFastJson(source,target);
        }else {
            SimplePropertyPreFilter filter = new SimplePropertyPreFilter();
            filter.getExcludes().addAll(ignoreProperties);
            String sourceStr = JSONObject.toJSONString(source,filter);
            return JSONObject.parseObject(sourceStr, target);
        }
    }


    /**
     * 使用fastJson，转换为json字符串再拷贝属性
     * 支持字段和枚举类转换
     *
     * @param source 源对象列表
     * @param target 目标对象的class
     * @return 目标对象实例列表
     */
    public static <T> List<T> copyPropertiesByFastJson(List<?> source, Class<T> target) {
        if (source == null) {
            return null;
        }
        return source.stream().map(item -> copyPropertiesByFastJson(item, target))
                .collect(Collectors.toList());
    }

    /**
     * 使用fastJson，转换为json字符串再拷贝属性
     * 支持字段和枚举类转换
     *
     * @param source 源对象列表
     * @param target 目标对象的class
     * @param ignoreProperties 忽略的对象
     * @return 目标对象实例列表
     */
    @Deprecated
    public static <T> List<T> copyPropertiesByFastJson(List<?> source, Class<T> target,String[] ignoreProperties) {
        if (source == null) {
            return null;
        }
        return source.stream().map(item -> copyPropertiesByFastJson(item, target ,ignoreProperties))
                .collect(Collectors.toList());
    }


    /**
     * 使用fastJson，转换为json字符串再拷贝属性
     * 支持字段和枚举类转换
     *
     * @param source 源对象列表
     * @param target 目标对象的class
     * @param ignoreProperties 忽略的对象
     * @return 目标对象实例列表
     */
    public static <T> List<T> copyPropertiesByFastJson(List<?> source, Class<T> target,List<String> ignoreProperties) {
        if (source == null) {
            return null;
        }
        return source.stream().map(item -> copyPropertiesByFastJson(item, target ,ignoreProperties))
                .collect(Collectors.toList());
    }
}