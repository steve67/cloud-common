package com.cloud.common.util;

import com.cloud.common.enums.RequestContextEnum;
import com.cloud.common.vo.RequestContext;

/**
 * Description :   .
 *
 * @author : frm
 * @date : Created in 2018/11/21 3:53 PM
 */
public class TokenUtil {

    public static String getActiveToken() {
        String hxToken = RequestContext.getContext().getContextDetail(RequestContextEnum.HX_TOKEN.name());
        String hxReturnToken = RequestContext.getContext().getContextDetail(RequestContextEnum.HX_RETURN_TOKEN.name());
        if (hxReturnToken != null) {
            return hxReturnToken;
        } else {
            return hxToken;
        }
    }
}
