package com.cloud.common.util;

import org.apache.commons.codec.binary.Base64;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

/**
 * 算法工具
 *
 * @author : frm
 * @date : Created in 2018/10/26 11:01 AM
 */
public class AlgorithmUtil {


    static final String CHARSET="utf-8";

    /**
     * MD5 算法
     *
     * @author : frm
     * @date : Created in 2018/10/26 11:11 AM
     */
    public static class Md5 {

        /**
         * 散列编码
         *
         * @param data 文本数据
         */
        public static String encode(String data) {
            MessageDigest digest;
            try {
                digest = MessageDigest.getInstance("MD5");
            } catch (NoSuchAlgorithmException e) {
                throw new IllegalStateException(
                        "MD5 algorithm not available.  Fatal (should be in the JDK).");
            }

            byte[] bytes = digest.digest(data.getBytes(StandardCharsets.UTF_8));
            return String.format("%032x", new BigInteger(1, bytes));
        }
    }

    /**
     * DES 算法
     *
     * @author : frm
     * @date : Created in 2018/10/26 11:11 AM
     */
    public static class Des {

        /**
         * 加密
         *
         * @param data 数据
         * @param key 密钥
         *
         * @return .
         *
         * @throws Exception .
         */
        public static String encrypt(String data, String key) throws Exception {
            byte[] bt = Des.encrypt(data.getBytes(), key.getBytes());
            return new BASE64Encoder().encode(bt);
        }

        /**
         * 解密
         *
         * @param data 加密数组
         * @param key 密钥
         *
         * @return .
         *
         * @throws IOException .
         * @throws Exception   .
         */
        public static String decrypt(String data, String key) throws IOException, Exception {
            if (data == null) {
                return null;
            }

            BASE64Decoder decoder = new BASE64Decoder();
            byte[] buf = decoder.decodeBuffer(data);
            byte[] bt = decrypt(buf, key.getBytes());

            return new String(bt);
        }

        /**
         * 加密
         *
         * @param data 数据
         * @param key 密钥
         *
         * @return .
         *
         * @throws Exception .
         */
        private static byte[] encrypt(byte[] data, byte[] key) throws Exception {
            // 生成一个可信任的随机数源
            SecureRandom sr = new SecureRandom();

            // 从原始密钥数据创建DESKeySpec对象
            DESKeySpec dks = new DESKeySpec(key);

            // 创建一个密钥工厂，然后用它把DESKeySpec转换成SecretKey对象
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            SecretKey secureKey = keyFactory.generateSecret(dks);

            // Cipher对象实际完成加密操作
            Cipher cipher = Cipher.getInstance("DES");

            // 用密钥初始化Cipher对象
            cipher.init(Cipher.ENCRYPT_MODE, secureKey, sr);

            return cipher.doFinal(data);
        }

        /**
         * 解密
         *
         * @param data 加密数据
         * @param key 密钥
         *
         * @return .
         *
         * @throws Exception .
         */
        private static byte[] decrypt(byte[] data, byte[] key) throws Exception {
            // 生成一个可信任的随机数源
            SecureRandom sr = new SecureRandom();

            // 从原始密钥数据创建DESKeySpec对象
            DESKeySpec dks = new DESKeySpec(key);

            // 创建一个密钥工厂，然后用它把DESKeySpec转换成SecretKey对象
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            SecretKey securekey = keyFactory.generateSecret(dks);

            // Cipher对象实际完成解密操作
            Cipher cipher = Cipher.getInstance("DES");

            // 用密钥初始化Cipher对象
            cipher.init(Cipher.DECRYPT_MODE, securekey, sr);

            return cipher.doFinal(data);
        }
    }

    /**
     * RSA 算法
     *
     * @author : frm
     * @date : Created in 2018/10/26 11:11 AM
     */
    public static class Rsa {

        /**
         * 获取公钥
         *
         * @param pubKey 公钥字符串
         *
         * @return .
         */
        public static PublicKey getPublicKey(String pubKey) {

            PublicKey publicKey = null;

            // 公钥
            X509EncodedKeySpec bobPubKeySpec;
            try {
                bobPubKeySpec = new X509EncodedKeySpec(new BASE64Decoder().decodeBuffer(pubKey));

                // RSA对称加密算法
                KeyFactory keyFactory = KeyFactory.getInstance("RSA");
                // 取公钥匙对象
                publicKey = keyFactory.generatePublic(bobPubKeySpec);
            } catch (IOException | NoSuchAlgorithmException | InvalidKeySpecException e) {
                e.printStackTrace();
            }
            return publicKey;
        }

        /**
         * 获取私钥
         *
         * @param priKey 私钥字符串
         */
        public static PrivateKey getPrivateKey(String priKey) {

            PrivateKey privateKey = null;

            // 私钥
            PKCS8EncodedKeySpec priPKCS8;
            try {
                priPKCS8 = new PKCS8EncodedKeySpec(new BASE64Decoder().decodeBuffer(priKey));

                // RSA堆成加密算法
                KeyFactory keyFactory = KeyFactory.getInstance("RSA");
                // 取私钥对象
                privateKey = keyFactory.generatePrivate(priPKCS8);
            } catch (IOException | NoSuchAlgorithmException | InvalidKeySpecException e) {
                e.printStackTrace();
            }

            return privateKey;
        }

        /**
         * 公钥加密
         *
         * @param data      数据
         * @param publicKey 公钥
         *
         * @return .
         */
        public static String publicEncrypt(String data, RSAPublicKey publicKey) {
            try {
                Cipher cipher = Cipher.getInstance("RSA");
                cipher.init(Cipher.ENCRYPT_MODE, publicKey);
                return Base64
                        .encodeBase64URLSafeString(
                                rsaSplitCodec(cipher, Cipher.ENCRYPT_MODE, data.getBytes(
                                        StandardCharsets.UTF_8),
                                        publicKey.getModulus().bitLength()));
            } catch (Exception e) {
                throw new RuntimeException("加密字符串[" + data + "]时遇到异常", e);
            }
        }

        /**
         * 私钥解密
         *
         * @param data       加密数据
         * @param privateKey 私钥
         *
         * @return .
         */

        public static String privateDecrypt(String data, RSAPrivateKey privateKey) {
            try {
                Cipher cipher = Cipher.getInstance("RSA");
                cipher.init(Cipher.DECRYPT_MODE, privateKey);
                return new String(rsaSplitCodec(cipher, Cipher.DECRYPT_MODE,
                        Base64.decodeBase64(data), privateKey.getModulus().bitLength()), CHARSET);
            } catch (Exception e) {
                throw new RuntimeException("私钥解密字符串[" + data + "]时遇到异常", e);
            }
        }

        /**
         * 私钥加密
         *
         * @param data       数据
         * @param privateKey 私钥
         */

        public static String privateEncrypt(String data, RSAPrivateKey privateKey) {
            try {
                Cipher cipher = Cipher.getInstance("RSA");
                cipher.init(Cipher.ENCRYPT_MODE, privateKey);
                return Base64.encodeBase64URLSafeString(
                        rsaSplitCodec(cipher, Cipher.ENCRYPT_MODE, data.getBytes(CHARSET),
                                privateKey.getModulus().bitLength()));
            } catch (Exception e) {
                throw new RuntimeException("加密字符串[" + data + "]时遇到异常", e);
            }
        }

        /**
         * 公钥解密
         *
         * @param data      加密数据
         * @param publicKey 公钥
         *
         * @return .
         */

        public static String publicDecrypt(String data, RSAPublicKey publicKey) {
            try {
                Cipher cipher = Cipher.getInstance("RSA");
                cipher.init(Cipher.DECRYPT_MODE, publicKey);
                return new String(rsaSplitCodec(cipher, Cipher.DECRYPT_MODE,
                        Base64.decodeBase64(data), publicKey.getModulus().bitLength()), CHARSET);
            } catch (Exception e) {
                throw new RuntimeException("公钥解密字符串[" + data + "]时遇到异常", e);
            }
        }

        /**
         * RSA密钥分割
         *
         * @param cipher  计算
         * @param opmode  模式
         * @param data    数据
         * @param keySize 密钥大小
         *
         * @return .
         */
        private static byte[] rsaSplitCodec(Cipher cipher, int opmode, byte[] data, int keySize) {
            int maxBlock = 0;
            if (opmode == Cipher.DECRYPT_MODE) {
                maxBlock = keySize / 8;
            } else {
                maxBlock = keySize / 8 - 11;
            }
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            int offSet = 0;
            byte[] buff;
            int i = 0;
            try {
                while (data.length > offSet) {
                    if (data.length - offSet > maxBlock) {
                        buff = cipher.doFinal(data, offSet, maxBlock);
                    } else {
                        buff = cipher.doFinal(data, offSet, data.length - offSet);
                    }
                    out.write(buff, 0, buff.length);
                    i++;
                    offSet = i * maxBlock;
                }
            } catch (Exception e) {
                throw new RuntimeException("加解密阀值为[" + maxBlock + "]的数据时发生异常", e);
            }

            byte[] resultData = out.toByteArray();
            try {
                out.close();
            }catch (IOException e){

            }
            return resultData;
        }
    }

    /**
     * SHA1 算法
     *
     * @author : frm
     * @date : Created in 2018/10/26 11:11 AM
     */
    public static class SHA1 {

        private static final char[] HEX_DIGITS = {'0', '1', '2', '3', '4', '5',
                '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

        /**
         * 格式化文本
         *
         * @param bytes 文本数据
         *
         * @return .
         */
        private static String getFormattedText(byte[] bytes) {
            int len = bytes.length;
            StringBuilder buf = new StringBuilder(len * 2);

            // 把密文转换成十六进制的字符串形式
            for (byte aByte : bytes) {
                buf.append(HEX_DIGITS[(aByte >> 4) & 0x0f]);
                buf.append(HEX_DIGITS[aByte & 0x0f]);
            }

            return buf.toString();
        }

        /**
         * 散列编码
         *
         * @param data Hash数据
         *
         * @return .
         */
        public static String encode(String data) {
            if (data == null) {
                return null;
            }

            try {
                MessageDigest messageDigest = MessageDigest.getInstance("SHA1");
                messageDigest.update(data.getBytes());
                return getFormattedText(messageDigest.digest());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * Unicode 编码
     *
     * @author : frm
     * @date : Created in 2018/10/26 11:11 AM
     */
    public static class Unicode {

        /**
         * Unicode 转为 UTF-8
         *
         * @param encodeString Unicode字符串
         */
        public static String toUtf8(String encodeString) {
            char aChar;
            int len = encodeString.length();
            StringBuilder outBuffer = new StringBuilder(len);
            for (int x = 0; x < len; ) {
                aChar = encodeString.charAt(x++);
                if (aChar == '\\') {
                    aChar = encodeString.charAt(x++);
                    if (aChar == 'u') {
                        // Read the xxxx
                        int value = 0;
                        for (int i = 0; i < 4; i++) {
                            aChar = encodeString.charAt(x++);
                            switch (aChar) {
                                case '0':
                                case '1':
                                case '2':
                                case '3':
                                case '4':
                                case '5':
                                case '6':
                                case '7':
                                case '8':
                                case '9':
                                    value = (value << 4) + aChar - '0';
                                    break;
                                case 'a':
                                case 'b':
                                case 'c':
                                case 'd':
                                case 'e':
                                case 'f':
                                    value = (value << 4) + 10 + aChar - 'a';
                                    break;
                                case 'A':
                                case 'B':
                                case 'C':
                                case 'D':
                                case 'E':
                                case 'F':
                                    value = (value << 4) + 10 + aChar - 'A';
                                    break;
                                default:
                                    throw new IllegalArgumentException(
                                            "Malformed   \\uxxxx   encoding.");
                            }
                        }
                        outBuffer.append((char) value);
                    } else {
                        if (aChar == 't') {
                            aChar = '\t';
                        } else if (aChar == 'r') {
                            aChar = '\r';
                        } else if (aChar == 'n') {
                            aChar = '\n';
                        } else if (aChar == 'f') {
                            aChar = '\f';
                        }
                        outBuffer.append(aChar);
                    }
                } else {
                    outBuffer.append(aChar);
                }
            }
            return outBuffer.toString();
        }
    }
}
