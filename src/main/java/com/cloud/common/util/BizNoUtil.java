package com.cloud.common.util;

import com.cloud.common.enums.BizNoPropertiesEnum;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * CreateBy KKYV 2018/9/18
 */
@Deprecated
public class BizNoUtil {

    /**
     * 获取流水号
     *
     * @param bizNum 业务号 范围:[0-99]
     * @param bizId 业务id 范围:[0-x]
     * @return 流水号
     */
    public static String getBizNo(Integer bizNum, Integer bizId) {
        if (bizNum == null || bizNum > 99 || bizNum < 0) {
            throw new IllegalArgumentException("流水号生成错误,业务号范围0~99");
        }
        if (bizId == null || bizId <= 0) {
            throw new IllegalArgumentException("流水号生成错误,业务id必须大于0");
        }
            // 业务号左边补0
        String bizNoStr = StringUtils.leftPad(bizNum.toString(), 2, '0');
        String bizIdStr = StringUtils.leftPad(bizId.toString(), 10, '0');
        return StringUtils.join(PropertiesUtil.getProperty("com.cloud.system-num"), bizNoStr, DateFormatUtils.format(new Date(), "yyyyMMdd"), bizIdStr);
    }

    /**
     * 解析流水号
     *
     * @param bizNo 流水号
     * @return 解析后的结果
     */
    public static Map<BizNoPropertiesEnum, Integer> parseBizNo(String bizNo) {
        if (bizNo == null || bizNo.length() < 5) {
            throw new IllegalArgumentException("流水号错误");
        }
        String sysNum = bizNo.substring(0, 3);
        String bizNum = bizNo.substring(3, 5);
        String date = bizNo.substring(5, 13);
        String bizId = bizNo.substring(13);

        Map<BizNoPropertiesEnum, Integer> map = new HashMap<>();
        // 系统号
        map.put(BizNoPropertiesEnum.SYSTEM_NUM, Integer.valueOf(sysNum));
        // 业务号
        map.put(BizNoPropertiesEnum.BIZ_NUM, Integer.valueOf(bizNum));
        map.put(BizNoPropertiesEnum.DATE, Integer.valueOf(date));
        // 业务ID
        map.put(BizNoPropertiesEnum.BIZ_ID, Integer.valueOf(bizId));

        return map;
    }

    /**
     * 获取流水号(美团雪花算法)
     *
     *  系统号-业务号-时间-库名-表名-预留位-业务id(唯一id加密处理后，防止进位多加一位)
     *  3-2-6-2-4-2-13 32
     *
     * @param bizNum 业务号       范围:[0-99]
     * @param databaseNum 库编号  范围:[0-99]
     * @param tableNum 表编号     范围:[0-1024]
     * @param reserved 预留位     范围:[0-99]
     * @param bizId 业务id        范围19位:(0-X]
     * @return 流水号
     */
    public static String getBizNoV1(Integer bizNum,
            Integer databaseNum, Integer tableNum,
            Integer reserved, Long bizId) {
        if (bizNum == null || bizNum > 99 || bizNum < 0) {
            throw new IllegalArgumentException("流水号生成错误,业务号范围[0~99]");
        }
        if (bizId == null || bizId <= 0) {
            throw new IllegalArgumentException("流水号生成错误,业务id必须大于0");
        }
        if (databaseNum == null || databaseNum < 0 || databaseNum > 99) {
            throw new IllegalArgumentException("流水号生成错误,库名编号范围[0~99]");
        }
        if (tableNum == null || tableNum < 0 || tableNum > 1024) {
            throw new IllegalArgumentException("流水号生成错误,表名编号范围[0~1024]");
        }
        if (reserved == null || reserved > 99 || reserved < 0) {
            throw new IllegalArgumentException("流水号生成错误,预留位范围0~99");
        }
        // 各部分长度确认 左边补0
        String bizNoStr = StringUtils.leftPad(bizNum.toString(), 2, '0');
        String dateStr = DateFormatUtils.format(new Date(), "yyMMdd");
        String databaseNoStr = StringUtils.leftPad(databaseNum.toString(), 2, '0');
        String tableNoStr = StringUtils.leftPad(tableNum.toString(), 4, '0');
        String reservedStr = StringUtils.leftPad(String.valueOf(reserved), 2, '0');
        String bizIdStr = StringUtils.leftPad(NumericConvertUtils.toOtherNumberSystem32(bizId), 13, '0');

        return StringUtils.join(PropertiesUtil.getProperty("com.cloud.system-num"), bizNoStr,
                dateStr, databaseNoStr, tableNoStr, reservedStr, bizIdStr);
    }

    /**
     * 解析流水号
     *
     *   系统号-业务号-时间-库名-表名-预留位-业务id(唯一id加密处理后，防止进位多加一位)
     *   3-2-6-2-4-2-13 32
     *
     * @param bizNo 流水号
     * @return 解析后的结果
     */
    public static Map<BizNoPropertiesEnum, Object> parseBizNoV1(String bizNo) {
        if (bizNo == null || bizNo.length() < 32) {
            throw new IllegalArgumentException("流水号错误");
        }
        String sysNum = bizNo.substring(0, 3);
        String bizNum = bizNo.substring(3, 5);
        String date = bizNo.substring(5, 11);
        String databaseNum = bizNo.substring(11, 13);
        String tableNum = bizNo.substring(13, 17);
        String reserved = bizNo.substring(17, 19);
        String bizId = bizNo.substring(19);

        Map<BizNoPropertiesEnum, Object> map = new HashMap<>(10);
        // 系统号
        map.put(BizNoPropertiesEnum.SYSTEM_NUM, Integer.valueOf(sysNum));
        // 业务号
        map.put(BizNoPropertiesEnum.BIZ_NUM, Integer.valueOf(bizNum));
        map.put(BizNoPropertiesEnum.DATE, Integer.valueOf(date));
        // 库名编号
        map.put(BizNoPropertiesEnum.DATABASE_NUM, Integer.valueOf(databaseNum));
        // 表名编号
        map.put(BizNoPropertiesEnum.TABLE_NUM, Integer.valueOf(tableNum));
        // 预留位
        map.put(BizNoPropertiesEnum.RESERVED, Integer.valueOf(reserved));
        // 业务ID
        map.put(BizNoPropertiesEnum.BIZ_ID, NumericConvertUtils.toDecimalNumber32(bizId.replace("0", "")));

        return map;
    }
}
