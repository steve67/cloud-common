package com.cloud.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Description:   .
 *
 * @author : yanggang
 * @date : Created in 2018-12-18 0018 19:35
 */
@Getter
@AllArgsConstructor
public enum  BizNoPropertiesEnum {

    SYSTEM_NUM("系统号"),
    BIZ_NUM("内部业务号"),
    DATE("日期"),
    DATABASE_NUM("库名编号"),
    TABLE_NUM("表名编号"),
    RESERVED("预留位"),
    BIZ_ID("内部业务ID"),
    ;

    private String desc;

}
