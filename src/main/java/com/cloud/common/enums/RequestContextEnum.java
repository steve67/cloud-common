package com.cloud.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by snlu on 2017/10/16. Rpc参数枚举
 */
@AllArgsConstructor
public enum RequestContextEnum {
    TRACE_ID("X-B3-TraceId", "php系统串号"),
    //    REQUEST_CONNECTION_TIME("REQUEST_CONNECTION_TIME", "外部request请求时间"),
//    REQUEST_TIME_OUT_SECOND("REQUEST_TIME_OUT_SECOND", "外部request请求超时时间"),
    HX_TOKEN("Authorization", "登录授权token"),
    HX_RETURN_TOKEN("Return_Authorization", "返回token"),
    HX_ROUTE_TAG("HX-ROUTE-TAG", "慧贤的流量路由标签"),
    ;

    @Getter
    private String outCode;

    @Getter
    private String desc;


    /**
     * 根据name获取枚举
     */
    public static RequestContextEnum getByName(String name) {
        for (RequestContextEnum item : RequestContextEnum.values()) {
            if (StringUtils.equalsIgnoreCase(name, item.name())) {
                return item;
            }
        }
        return null;
    }

    /**
     * 根据name获取枚举
     */
    public static RequestContextEnum getByOutCode(String outCode) {
        for (RequestContextEnum item : RequestContextEnum.values()) {
            if (StringUtils.equalsIgnoreCase(outCode, item.getOutCode())) {
                return item;
            }
        }
        return null;
    }
}
